# Esperanto translation for content-hub
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the content-hub package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: content-hub\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 15:40+0000\n"
"PO-Revision-Date: 2024-01-04 16:35+0000\n"
"Last-Translator: phlostically <phlostically@mailinator.com>\n"
"Language-Team: Esperanto <https://hosted.weblate.org/projects/lomiri/"
"content-hub/eo/>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.4-dev\n"
"X-Launchpad-Export-Date: 2016-12-01 04:56+0000\n"

#: src/com/lomiri/content/detail/service.cpp:238
msgid "Download Complete"
msgstr "Elŝuto finiĝis"

#: src/com/lomiri/content/detail/service.cpp:256
msgid "Open"
msgstr "Malfermi"

#: src/com/lomiri/content/detail/service.cpp:263
msgid "Dismiss"
msgstr "Rezigni"

#: src/com/lomiri/content/detail/service.cpp:281
msgid "Download Failed"
msgstr "Elŝuto malsukcesis"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Choose from"
msgstr "Elekti el"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Open with"
msgstr "Malfermi per"

#: import/Lomiri/Content/ContentPeerPicker10.qml:50
#: import/Lomiri/Content/ContentPeerPicker13.qml:51
#: import/Lomiri/Content/ContentPeerPicker11.qml:51
msgid "Share to"
msgstr "Konigi al"

#: import/Lomiri/Content/ContentPeerPicker10.qml:162
#: import/Lomiri/Content/ContentPeerPicker13.qml:170
#: import/Lomiri/Content/ContentPeerPicker11.qml:180
msgid "Apps"
msgstr "Aplikaĵoj"

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can provide this type "
"of content."
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:198
#: import/Lomiri/Content/ContentPeerPicker13.qml:207
#: import/Lomiri/Content/ContentPeerPicker11.qml:216
msgid ""
"Sorry, there aren't currently any apps installed that can handle this type "
"of content."
msgstr ""

#: import/Lomiri/Content/ContentPeerPicker10.qml:214
#: import/Lomiri/Content/ContentPeerPicker13.qml:223
#: import/Lomiri/Content/ContentPeerPicker11.qml:232
msgid "Devices"
msgstr "Aparatoj"

#: import/Lomiri/Content/ContentPeerPicker10.qml:251
#: import/Lomiri/Content/ContentPeerPicker11.qml:59
#: import/Lomiri/Content/ContentTransferHint.qml:65
msgid "Cancel"
msgstr "Nuligi"

#: import/Lomiri/Content/ContentTransferHint.qml:52
msgid "Transfer in progress"
msgstr "Transdonante"

#: examples/picker-qml/picker.qml:21
msgid "Peer Picker Example"
msgstr ""

#: examples/picker-qml/picker.qml:37
msgid "Sources"
msgstr "Fontoj"

#: examples/picker-qml/picker.qml:39
msgid "Select source"
msgstr "Elektu fonton"

#: examples/picker-qml/picker.qml:52
msgid "Results"
msgstr "Rezultoj"
