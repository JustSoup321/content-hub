# Copyright © 2013 Canonical Ltd.
# Copyright © 2022 UBports Foundation.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set(QT_IMPORTS_DIR "${CMAKE_INSTALL_LIBDIR}/qt5/qml")
set(CONTENT_HUB_IMPORTS_DIR "${QT_IMPORTS_DIR}/Ubuntu/Content")

set(PLUGIN "ubuntu-content-hub-plugin")

add_custom_target(${PLUGIN} ALL
  COMMAND ${CMAKE_COMMAND} -E create_symlink 
    ../../Lomiri/Content/liblomiri-content-hub-plugin.so
    ${CMAKE_CURRENT_BINARY_DIR}/libubuntu-content-hub-plugin.so
  BYPRODUCTS ${CMAKE_CURRENT_BINARY_DIR}/libubuntu-content-hub-plugin.so)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libubuntu-content-hub-plugin.so
  DESTINATION ${CONTENT_HUB_IMPORTS_DIR})

file(GLOB QML_FILES RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.qml *.js qmldir)
install(FILES ${QML_FILES} DESTINATION ${CONTENT_HUB_IMPORTS_DIR})

if(NOT ${CMAKE_CURRENT_BINARY_DIR} STREQUAL ${CMAKE_CURRENT_SOURCE_DIR})
  # copy qml files and assets over to build dir to be able to import them uninstalled
  foreach(_file ${QML_FILES})
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${_file}
                       DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_file}
                       COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_CURRENT_SOURCE_DIR}/${_file} ${CMAKE_CURRENT_BINARY_DIR}/${_file})
  endforeach(_file)
  add_custom_target(copy_ubuntu_compat_files_to_build_dir DEPENDS ${QML_FILES})
endif()
