/*
 * Copyright 2013 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "qmlimportexporthandler.h"
#include "../../../src/com/lomiri/content/debug.h"

#include <com/lomiri/content/transfer.h>

namespace cuc = com::lomiri::content;

/*!
 * QmlImportExportHandler is for internal use only
 */

QmlImportExportHandler::QmlImportExportHandler(QObject *parent)
    : cuc::ImportExportHandler(parent)
{
    TRACE() << Q_FUNC_INFO;
}

/*!
 * \reimp
 */
void QmlImportExportHandler::handle_import(com::lomiri::content::Transfer *transfer)
{
    TRACE() << Q_FUNC_INFO;
    Q_EMIT importRequested(transfer);
}

/*!
 * \reimp
 */
void QmlImportExportHandler::handle_export(com::lomiri::content::Transfer *transfer)
{
    TRACE() << Q_FUNC_INFO;
    Q_EMIT exportRequested(transfer);
}

/*!
 * \reimp
 */
void QmlImportExportHandler::handle_share(com::lomiri::content::Transfer *transfer)
{
    TRACE() << Q_FUNC_INFO;
    Q_EMIT shareRequested(transfer);
}
